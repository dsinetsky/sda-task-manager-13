package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectService;
import ru.t1.dsinetsky.tm.enumerated.Status;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> returnAll() {
        return projectRepository.returnAll();
    }

    @Override
    public Project create(final String name) {
        if (name == null && name.isEmpty()) return null;
        return add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return add(new Project(name, description));
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return projectRepository.add(project);
    }

    @Override
    public void clearAll() {
        projectRepository.clearAll();
    }

    @Override
    public Project findById(String id) {
        if (id != null && !id.isEmpty()) return projectRepository.findById(id);
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        if (index < 0) return null;
        return projectRepository.findByIndex(index);
    }

    @Override
    public Project removeById(String id) {
        if (id != null && !id.isEmpty()) return projectRepository.removeById(id);
        return null;
    }

    @Override
    public Project removeByIndex(int index) {
        if (index < 0) return null;
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = findById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project updateByIndex(int index, String name, String description) {
        if (index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        Project project = findByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDesc(description);
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        Project project = findById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(int index, Status status) {
        if (index < 0) return null;
        if (index >= projectRepository.getSize()) return null;
        Project project = findByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public void createTestProjects() {
        String name = "project";
        String description = "desc";
        for (int i = 1; i < 11; i++) {
            create(name + i, description + i);
        }
    }

}
