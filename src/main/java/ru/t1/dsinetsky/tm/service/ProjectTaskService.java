package ru.t1.dsinetsky.tm.service;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.api.repository.ITaskRepository;
import ru.t1.dsinetsky.tm.api.service.IProjectTaskService;
import ru.t1.dsinetsky.tm.model.Project;
import ru.t1.dsinetsky.tm.model.Task;

import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindProjectById(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(projectId);
    }

    @Override
    public void unbindProjectById(String projectId, String taskId) {
        if (projectId == null || projectId.isEmpty()) return;
        if (taskId == null || taskId.isEmpty()) return;
        if (!projectRepository.existsById(projectId)) return;
        final Task task = taskRepository.findById(taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        final Project project = projectRepository.findById(projectId);
        if (project == null) return null;
        final List<Task> tasks = taskRepository.findTasksByProjectId(projectId);
        for (Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeById(projectId);
        return project;
    }

    @Override
    public Project removeProjectByIndex(int index) {
        if (index < 0) return null;
        final Project project = projectRepository.findByIndex(index);
        if (project == null) return null;
        final List<Task> tasks = taskRepository.findTasksByProjectId(project.getId());
        for (Task task : tasks) taskRepository.removeById(task.getId());
        projectRepository.removeByIndex(index);
        return project;
    }


}
