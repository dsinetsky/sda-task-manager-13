package ru.t1.dsinetsky.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTasksByProjectId();

    void updateTaskById();

    void updateTaskByIndex();

    void removeTaskById();

    void removeTaskByIndex();

    void createTestTasks();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeStatusById();

    void changeStatusByIndex();

}
