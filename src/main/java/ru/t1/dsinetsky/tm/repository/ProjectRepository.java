package ru.t1.dsinetsky.tm.repository;

import ru.t1.dsinetsky.tm.api.repository.IProjectRepository;
import ru.t1.dsinetsky.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public List<Project> returnAll() {
        return projects;
    }

    @Override
    public Project add(final Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clearAll() {
        projects.clear();
    }

    @Override
    public Project findById(String id) {
        for (final Project project : projects) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project findByIndex(int index) {
        if (index >= projects.size()) return null;
        return projects.get(index);
    }

    @Override
    public Project removeById(String id) {
        Project project = findById(id);
        if (project == null) return null;
        else projects.remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(int index) {
        Project project = findByIndex(index);
        if (project == null) return null;
        else projects.remove(project);
        return project;
    }

    @Override
    public boolean existsById(String id) {
        return findById(id) != null;
    }

    @Override
    public int getSize() {
        return projects.size();
    }
}
